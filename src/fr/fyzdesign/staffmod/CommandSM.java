package fr.fyzdesign.staffmod;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

public class CommandSM implements CommandExecutor {

	private Main main;

	public CommandSM(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			Player p = (Player)sender;
			if(cmd.getName().equalsIgnoreCase("staffmod") || cmd.getName().equalsIgnoreCase("sm") || cmd.getName().equalsIgnoreCase("mod")) {
				if(p.hasPermission("staffmod.use")) {
					if(!main.mod.contains(p)) {
						main.mod.add(p);
						main.invsave.put(p, p.getInventory().getContents());
						main.invsavearmor.put(p, p.getInventory().getArmorContents());
						p.getInventory().clear();
						p.getInventory().setItem(0, new ItemCreator(Material.WOOD_SWORD, 0).setName("�eKnockback tester").addEnchantment(Enchantment.KNOCKBACK, 5).getItem());
						p.getInventory().setItem(1, new ItemCreator(Material.CHEST, 0).setName("�eInventory Checker").getItem());
						p.getInventory().setItem(2, new ItemCreator(Material.ICE, 0).setName("�eFreeze Player").getItem());
						p.getInventory().setItem(3, new ItemCreator(Material.ANVIL, 0).setName("�eKick Player").getItem());
						p.getInventory().setItem(4, new ItemCreator(Material.PAPER, 0).setName("�eInformations from Player").getItem());
						p.getInventory().setItem(5, new ItemCreator(Material.REDSTONE, 0).setName("�eKill Player").getItem());
						p.getInventory().setItem(6, new ItemCreator(Material.HOPPER, 0).setName("�eClear Player").getItem());
						p.setGameMode(GameMode.CREATIVE);
						p.sendMessage("�aYou are now in �eStaffMod �a!");
						for (Player pls : Bukkit.getOnlinePlayers()) {
							pls.hidePlayer(p);
						}
					}
					else {
						main.mod.remove(p);
						p.getInventory().clear();
							p.getInventory().setContents(main.invsave.get(p));
							p.getInventory().setArmorContents(main.invsavearmor.get(p));
							main.invsave.remove(p);
							main.invsavearmor.remove(p);
							p.sendMessage("�cYou leave �eStaffMod �c!");
							p.setGameMode(GameMode.SURVIVAL);
							for (Player pls : Bukkit.getOnlinePlayers()) {
								pls.showPlayer(p);
							}
					}
				}
				else {
					p.sendMessage("�cHey ! You cant use that !");
				}
			}
			
			
		}
		return false;
	}

}
