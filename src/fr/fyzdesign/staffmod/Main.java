/*
 *    ______         _____            _             
 |  ____|       |  __ \          (_)            
 | |__ _   _ ___| |  | | ___  ___ _  __ _ _ __  
 |  __| | | |_  / |  | |/ _ \/ __| |/ _` | '_ \ 
 | |  | |_| |/ /| |__| |  __/\__ \ | (_| | | | |
 |_|   \__, /___|_____/ \___||___/_|\__, |_| |_|
        __/ |                        __/ |      
       |___/                        |___/       
 * All credits goes to FyzDesign.
 * Please don't use my code at your name, you can edit or look the code but please don't remove the credits.
 * 
 * Discord : FyzDesign#0309
 * Youtube : https://www.youtube.com/user/Mrfuri54
 * Spigot : https://www.spigotmc.org/members/fyzdesign.814195/
 * 
 * */
package fr.fyzdesign.staffmod;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Main extends JavaPlugin implements Listener{

	public void freeze(Player p){
		freeze.add(p);
		p.setWalkSpeed(0.0F);
		p.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, 100000, 128));
	}
	
	public void unFreeze(Player p){
		freeze.remove(p);
		p.setWalkSpeed(0.2F); //normal walking speed
		p.removePotionEffect(PotionEffectType.JUMP);
	}
	
	
	
	
	@EventHandler
	public void onD(PlayerDeathEvent e) {
		Player k = e.getEntity().getKiller();
		if(mod.contains(k)) {
			e.setDeathMessage(null);
		}
	}
	
	
	ArrayList<Player> freeze = new ArrayList<Player>();
	ArrayList<Player> mod = new ArrayList<Player>();
	public HashMap<Player, ItemStack[]> invsave = new HashMap<Player, ItemStack[]>();
	public HashMap<Player, ItemStack[]> invsavearmor= new HashMap<Player, ItemStack[]>();
	@Override
	public void onEnable() {
		System.out.println("Version "+getDescription().getVersion()+" | Made by FyzDesign ! | Need help ? : https://www.spigotmc.org/members/fyzdesign.814195/");
		getCommand("staffmod").setExecutor(new CommandSM(this));
		getCommand("configsm").setExecutor(new CommandConfig(this));
		this.getServer().getPluginManager().registerEvents(this, this);
		saveDefaultConfig();
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e) {
		if(freeze.contains(e.getPlayer())) {
			e.setCancelled(true);
			e.getPlayer().sendMessage("�cYou are frozen.");
		}
	}
	
	@EventHandler
	public void onPlayerInteract(PlayerInteractEntityEvent e){
		if(mod.contains(e.getPlayer())) {
		Player player = e.getPlayer();
		Entity entity = e.getRightClicked();
		if(player.getItemInHand().getType() == Material.ICE) {
			if(entity instanceof Player) {
				if(!(freeze.contains((Player)entity))) {
					freeze((Player) entity);
					player.sendMessage("�a"+entity.getName()+" is now freeze !");
				}
				else {
					unFreeze((Player) entity);
					player.sendMessage("�c"+entity.getName()+" is free !");
				}
			}
		}
		else if(player.getItemInHand().getType() == Material.CHEST) {
			if(entity instanceof Player) {
				player.openInventory(((Player) entity).getInventory());
				player.sendMessage("�aWoosh !");
			}
		}
		else if(player.getItemInHand().getType() == Material.REDSTONE) {
			if(entity instanceof Player) {
				((Player) entity).setHealth(00);
				player.sendMessage("�aPlayer "+entity.getName()+ " �akilled.");
			}
		}
		else if(player.getItemInHand().getType() == Material.HOPPER) {
			if(entity instanceof Player) {
				((Player) entity).getInventory().clear();
				((Player) entity).getInventory().setHelmet(null);
				((Player) entity).getInventory().setChestplate(null);
				((Player) entity).getInventory().setLeggings(null);
				((Player) entity).getInventory().setBoots(null);
				player.sendMessage("�a"+entity.getName()+"�a have been clear.");
			}
		}
		else if(player.getItemInHand().getType() == Material.ANVIL) {
			if(entity instanceof Player) {
				((Player) entity).kickPlayer("�aYou have been kicked from the server !");
				player.sendMessage("�aPlayer have been kicked.");
			}
		}
		else if(player.getItemInHand().getType() == Material.PAPER) {
			if(entity instanceof Player) {
				player.sendMessage("�eInformation from "+entity.getName());
				player.sendMessage("�ePing of player : "+Math.round(((CraftPlayer)entity).getHandle().ping));
				player.sendMessage("�eUUID : "+entity.getUniqueId());
				player.sendMessage("�eHealth : "+((Player)entity).getHealth());
				player.sendMessage("�eFood level : "+((Player)entity).getFoodLevel());
				player.sendMessage("�ePlayer distance between you : "+Math.round(entity.getLocation().distance(player.getLocation())));
				player.sendMessage("�7�oMore in future update !");
			}
		}
	}
}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e) {
		if(mod.contains(e.getPlayer())) { 
			e.setCancelled(true); 
		}
	}
	
	@EventHandler
	public void onPickup(PlayerPickupItemEvent e) {
		if(mod.contains(e.getPlayer())) { 
			e.setCancelled(true); 
		}
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		for(Player inmod : mod) {
			e.getPlayer().hidePlayer(inmod);
		}
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		if(mod.contains(e.getPlayer())) { 
			e.setCancelled(true); 
		}
	}
	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		if(mod.contains(e.getPlayer())) {
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		if(mod.contains(e.getPlayer())) {
			e.getPlayer().getInventory().setContents(invsave.get(e.getPlayer()));
			e.getPlayer().getInventory().setArmorContents(invsavearmor.get(e.getPlayer()));
			e.getPlayer().setGameMode(GameMode.SURVIVAL);
			invsave.remove(e.getPlayer());
			mod.remove(e.getPlayer());
		}
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		if(e.getPlayer().hasPermission("staffmod.use")) {
		if(e.getMessage().length() > getConfig().get("staffchatprefix").toString().length()) {
		String charat = e.getMessage().substring(0, getConfig().getString("staffchatprefix").length());
		if(charat.equals(getConfig().getString("staffchatprefix"))) {
			for(Player pls : Bukkit.getOnlinePlayers()) {
				if(pls.hasPermission("staffmod.use")) {
					e.setCancelled(true);
					pls.sendMessage("�8[�cSC�8] �c"+e.getPlayer().getName()+" �8> �c"+e.getMessage().substring(getConfig().getString("staffchatprefix").length()));
				}
			}
		}
		}
		}
	}
}
