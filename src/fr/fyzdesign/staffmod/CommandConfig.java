package fr.fyzdesign.staffmod;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandConfig implements CommandExecutor {

	private Main main;

	public CommandConfig(Main main) {
		this.main = main;
	}

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(sender instanceof Player) {
			if(cmd.getName().equalsIgnoreCase("configsm")) {
				if(sender.isOp()) {
				if(args.length == 2) {
					if(args[0].equals("prefix")) {
						if(args.length == 2) {
						main.getConfig().set("staffchatprefix", args[1]);
						sender.sendMessage("�aSuccess ! The prefix for the staff chat is now : �e"+args[1]);
						main.saveConfig();
						}
						else {
						sender.sendMessage("�cError : You have to enter a new prefix. Usage : /configsm prefix [newprefix]");
						}
					}
				}
				if(args.length == 1) {
					if(args[0].equals("getprefix")) {
						if(args.length == 1) {
						sender.sendMessage("�aThe actual prefix is : �e"+main.getConfig().get("staffchatprefix"));
						}
						else {
							sender.sendMessage("�cError : Please enter a argument, you can use : getprefix (to get the actual prefix), prefix [new prefix] (to add another prefix)");
						}
					}
					}
				if(args.length == 0) {
					sender.sendMessage("�cError : Please enter a argument, you can use : getprefix (to get the actual prefix), prefix [new prefix] (to add another prefix)");
				}
				}
			}
		}
		return false;
	}
}